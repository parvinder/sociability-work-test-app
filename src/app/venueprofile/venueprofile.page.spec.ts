import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VenueprofilePage } from './venueprofile.page';

describe('VenueprofilePage', () => {
  let component: VenueprofilePage;
  let fixture: ComponentFixture<VenueprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenueprofilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VenueprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
