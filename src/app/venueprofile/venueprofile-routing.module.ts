import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VenueprofilePage } from './venueprofile.page';

const routes: Routes = [
  {
    path: '',
    component: VenueprofilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VenueprofilePageRoutingModule {}
