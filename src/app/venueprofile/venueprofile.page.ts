import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@capacitor/storage';
import { PhotosService } from "../services/photos/photos.service";
import { ReviewsService } from "../services/reviews/reviews.service";

@Component({
    selector: 'app-venueprofile',
    templateUrl: './venueprofile.page.html',
    styleUrls: ['./venueprofile.page.scss'],
})
export class VenueprofilePage implements OnInit {
    bannerSlider = {
        initialSlide: 0,
        speed: 400,
        slidesPerView: 1,
        spaceBetween: 2,
    };
    slidegallery = {
        initialSlide: 0,
        speed: 400,
        slidesPerView: 2.7,
        spaceBetween: 16,
    };
    slidereview = {
        initialSlide: 0,
        speed: 400,
        slidesPerView: 1.3,
        spaceBetween: 16,
    };

    public BtnClick: boolean = true;
    public photoData: any = [];
    public reviewData: any = [];

    constructor(
        public router: Router,
        private photosService: PhotosService,
        private reviewsService: ReviewsService,
    ) { }

    ngOnInit() {

    }

    async ionViewWillEnter() {
        await this.loadPhotos();
        await this.loadReviews();
    }

    async loadPhotos() {

        const { value } = await Storage.get({ key: 'photoData' });

        if (value) {
            this.photoData = JSON.parse(value);
        } else {
            this.photosService.fetchPhotos().subscribe(
                async data => {
                    this.photoData = data;

                    await Storage.set({
                        key: 'photoData',
                        value: JSON.stringify(data)
                    });

                    return;
                }, async err => {
                }
            );
        }
    }

    async loadReviews() {

        const { value } = await Storage.get({ key: 'reviewData' });

        if (value) {
            this.reviewData = JSON.parse(value);
        } else {
            this.reviewsService.fetchReviews().subscribe(
                async data => {
                    this.reviewData = data;

                    await Storage.set({
                        key: 'reviewData',
                        value: JSON.stringify(data)
                    });

                    return;
                }, async err => {
                }
            );
        }


    }
}
