import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VenueprofilePageRoutingModule } from './venueprofile-routing.module';

import { VenueprofilePage } from './venueprofile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VenueprofilePageRoutingModule
  ],
  declarations: [VenueprofilePage]
})
export class VenueprofilePageModule {}
