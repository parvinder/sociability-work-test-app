import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'venueprofile',
    loadChildren: () => import('./venueprofile/venueprofile.module').then( m => m.VenueprofilePageModule)
  },  
  {
    path: '',
    redirectTo: 'venueprofile',
    pathMatch: 'full'
  },  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
