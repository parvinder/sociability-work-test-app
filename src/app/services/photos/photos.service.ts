import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PhotosService {

  readonly PHOTO_DATA = '/photos';

  constructor(
    private http: HttpClient,
  ) {

  }

  fetchPhotos() {
    let url = environment.API_SERVER_URL +
      environment.API_PATH +
      this.PHOTO_DATA;

    return this.http.get(
      url
    );

  }

}
