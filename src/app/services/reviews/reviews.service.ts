import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ReviewsService {

  readonly REVIEW_DATA = '/reviews';

  constructor(
    private http: HttpClient,

  ) {

  }

  fetchReviews() {
    let url = environment.API_SERVER_URL +
      environment.API_PATH +
      this.REVIEW_DATA;

    return this.http.get(
      url
    );

  }

}
